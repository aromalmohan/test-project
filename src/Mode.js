

import  { useState } from 'react'
import { ThemeProvider, createTheme } from "@mui/material/styles";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';
const ThemeToggle = () => {
    const [light, setLight] = useState(true);
    const themeLight = createTheme({
        palette: {
            background: {
                default: "#ffffff"
            }
        }
    });
    const themeDark = createTheme({
        palette: {
            background: {
                default: "#393E46"
            },
            text: {
                default: "#FFFFFF"
            }
        }
    });
    return (
        <div className='bG'>
            <ThemeProvider theme={light ? themeLight : themeDark}>
                <CssBaseline />
                <Button style={{
                    borderRadius: 4,
                    backgroundColor: "#393E46",
                    padding: "5px 5px",
                    fontSize: "14px"
                }} variant="contained" onClick={() => setLight((prev) => !prev)}>{light ? <DarkModeIcon /> : <LightModeIcon />}</Button>
            </ThemeProvider>
        </div>
    )
}
export default ThemeToggle









