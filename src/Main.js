
import './App.css';
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
function App() {
  const navigate=useNavigate()
  const submit = (value) => {
    value.preventDefault()
    let date=(new Date(value.target.date_of_birth.value).getTime());
    axios.post('https://dev.hrmnest.com/v1/addDependents',
      {
        "relation": value.target.relation.value,
        "name": value.target.name.value,
        "gender": value.target.gender.value,
        "contactno": "7994912842",
        "employee": "dacda18c-ebaa-4f25-8666-44b42e750c4c",
        "date_of_birth": date,
      }).then((res) => {console.log(res.data.uuid)
        localStorage.setItem('id',res.data.uuid)
      })
      console.log(date);
      
      navigate('/view')
   }

  return (

    <div className="main">
      
      <div className='ring-gray-300 border-2 bg-slate-100 drop-shadow-lg rounded-md w-2/4 h-full flex flex-col ...items-center justify-center'>
        <br />
        <h1 className='text-center font-extrabold font text-4xl text-red-300'> Form</h1>
        <br />
        {/* <button class="bg-blue-500 shadow-lg shadow-blue-500/50 ... rounded-full w-12 py-2 ml-96">
            view
          </button> */}
        <div className='border bottom-1'></div>
        <br /><br />
        <form onSubmit={submit}>
          <div class="grid grid-cols-3 gap-4 h-12">
            <div class="...  px-16 py-1">Name</div>
            <div class="col-span-2 ... px-8">
              <div class="flex justify-center">
                <div class="mb-3 mt-1 xl:w-96">
                  <input
                    type="text"
                    class="
        form-control
        block
        w-full
        px-3 
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
                    id="exampleFormControlInput1"
                    placeholder=""
                    name='name'
                  />
                </div>
              </div>
            </div>
          </div>
          <div class="grid grid-cols-3 gap-4 h-12">
            <div class="... px-16 py-1">Gender </div>
            <div class="col-span-2 ... px-8">
              <div class="flex justify-center">
                <div class="mb-3 mt-1 xl:w-96">
                  <input
                    type="text"
                    class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
                    id="exampleFormControlInput1"
                    placeholder=""
                    name='gender'
                  />
                </div>
              </div>
            </div>
          </div>
          <div class="grid grid-cols-3 gap-4 h-12">
            <div class="... px-16 py-1">Relation </div>
            <div class="col-span-2 ... px-8 "><div class="flex justify-center">
              <div class="mb-3 mt-1 xl:w-96">
                <input
                  type="text"
                  class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
                  id="exampleFormControlInput1"
                  placeholder=""
                  name='relation'
                />
              </div>
            </div></div>
          </div>
          
          <div class="grid grid-cols-3 gap-4 h-12">
            <div class="...  px-16 py-1">DOB</div>
            <div class="col-span-2 ... px-8"><div class="flex justify-center">
              <div class="mb-3 mt-1 xl:w-96">
                <input
                  type="date"
                  class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
                  id="exampleFormControlInput1"
                  placeholder=""
                  name='date_of_birth'
                />
              </div>
            </div>
            </div>
          </div>

          <br />
          <br />
          <button class="bg-blue-500 shadow-lg shadow-blue-500/50 ... rounded-full w-1/3 py-2 mx-52 ...">
            Submit
          </button>
        </form>
         <br />
      </div>
    </div>
  )
}
export default App;
