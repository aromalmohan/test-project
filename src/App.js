import React from 'react'
import Mode from './Mode'
import Main from './Main'
import Update from './Update'
import {BrowserRouter as Router,Routes,Route} from 'react-router-dom'
import View from './View'

const App = () => {
  return (
    <div className='App'>
    <Router>
    <Mode/>
      
        <Routes>
         
         <Route path='/' element={<Main/>}/>
         <Route path='/view' element={<View/>}/>
         <Route path='/update' element={<Update/>}/>

     
      </Routes>
     
    
    </Router>
    </div>
  )
}

export default App