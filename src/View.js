import axios from 'axios'
import React, { useState } from 'react'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

const View = () => {
 const vari=localStorage.getItem('id')
 const navigate=useNavigate()
//  const [show, setShow] = useState(true)
 const [display,setDisplay]=useState({}
    // name:"",
    // relation:"",
    // gender:"",
    // date_of_birth:""
 )
console.log("view vari",vari)
useEffect(() => {
    axios.post('https://dev.hrmnest.com/v1/viewDependents', {
        "uuid" : vari
       })
       .then((res) => { setDisplay( res.data[0])
       })},[vari])
    const deleteId =()=>{
        axios
      .delete(`https://dev.hrmnest.com/v1/deleteDependents/${vari}`)
      .then(localStorage.clear())
      alert('sucessfully deleted')
      navigate('/')
    }
    // let theDate = new Date(display.date_of_birth);
    console.log("view display",display)
    // console.log(show);


  return (

    <div className='main'>
<div class="w-full max-w-md p-4 bg-white border rounded-lg shadow-md sm:p-8 dark:bg-gray-800 dark:border-gray-700">
    <div class="flex items-center justify-between mb-4">
        <h5 class="text-xl font-bold leading-none text-gray-900 dark:text-white">User Details</h5>
   </div>
   <div class="flow-root">
        <ul role="list" class="divide-y divide-gray-200 dark:divide-gray-700">
            <li class="py-3 sm:py-4">
                <div class="flex items-center space-x-4">
                    <div class="flex-shrink-0">
                    </div>
                    <div class="flex-1 min-w-0">
                        <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                            Name
                        </p>
                        <p class="text-sm text-gray-500 truncate dark:text-gray-400">
                           {display?.name}
                        </p>
                    </div>
                </div>
            </li>
            <li class="py-3 sm:py-4">
                <div class="flex items-center space-x-4">
                    <div class="flex-shrink-0">
                    </div>
                    <div class="flex-1 min-w-0">
                        <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                            Gender
                        </p>
                        <p class="text-sm text-gray-500 truncate dark:text-gray-400">
                        {display?.gender}

                        </p>
                    </div>
                </div>
            </li>
            <li class="py-3 sm:py-4">
                <div class="flex items-center space-x-4">
                    <div class="flex-shrink-0">
                    </div>
                    <div class="flex-1 min-w-0">
                        <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                            Relation
                        </p>
                        <p class="text-sm text-gray-500 truncate dark:text-gray-400">
                        {display?.relation}

                        </p>
                    </div>
                </div>
            </li>
            <li class="py-3 sm:py-4">
                <div class="flex items-center space-x-4">
                    <div class="flex-shrink-0">
                    </div>
                    <div class="flex-1 min-w-0">
                        <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                            DOB
                        </p>
                        <p class="text-sm text-gray-500 truncate dark:text-gray-400">
                        {display?.date_of_birth}
                        {/* {theDate} */}

                        </p>
                    </div>
                </div>
            </li>
    
        </ul>
        <button onClick={()=>(navigate('/'))} class="bg-blue-500 shadow-lg shadow-blue-500/50 ... rounded-full w-1/3 m-auto py-2">
            Go back
          </button>
        <button onClick={deleteId} class="bg-blue-500 shadow-lg shadow-blue-500/50 ... rounded-full w-1/3 m-auto py-2">
            Delete
          </button>
          <button onClick={()=>(navigate('/update'))} class="bg-blue-500 shadow-lg shadow-blue-500/50 ... rounded-full w-1/3 m-auto py-2">
            Edit
          </button>
         
   </div>
</div>
    

    </div>
  )
}

export default View

